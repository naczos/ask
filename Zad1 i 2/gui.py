#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import (QWidget, QGridLayout,
                             QPushButton, QApplication, QLabel, QComboBox, QLineEdit, QPlainTextEdit, QMessageBox)
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QPushButton, QAction

import win32clipboard as win32c
import numpy as np

from PyQt5.QtCore import QTime, QTimer
from PyQt5.QtWidgets import QApplication, QLCDNumber
from PyQt5.QtCore import QDate, QTime, QDateTime, Qt
import pyperclip
import os

class DigitalData(QLCDNumber):
    def __init__(self, parent=None):
        super(DigitalData, self).__init__(parent)

        self.setSegmentStyle(QLCDNumber.Filled)
        self.now = QDate.currentDate()

        self.data = self.now.toString(Qt.ISODate)
        self.showData(self.data)
        print("OTO JEST data: ", self.data)


        self.setWindowTitle("Digital Data")
        self.resize(150, 60)

    def showData(self, data2):
        text = data2[:2] + ' ' + data2[3:]
        self.display(text)

class DigitalClock(QLCDNumber):
    def __init__(self, parent=None):
        super(DigitalClock, self).__init__(parent)

        self.setSegmentStyle(QLCDNumber.Filled)
        self.text=''

        timer = QTimer(self)
        timer.timeout.connect(self.showTime)
        timer.start(1000)

        self.showTime()

        self.setWindowTitle("Digital Clock")
        self.resize(150, 60)


    def showTime(self):
        time = QTime.currentTime()
        #print("oto czas: ",time)
        self.text = time.toString('hh:mm')
        self.czas = time.toString('hh:mm')
        #print("Oto czas: ",self.czas)
        if (time.second() % 2) == 0:
            self.text = self.text[:2] + ' ' + self.text[3:]

        self.display(self.text)

#Twozenie pustych rejestrow
AH=np.zeros(8)
AL=np.zeros(8)
BH=np.zeros(8)
BL=np.zeros(8)
CH=np.zeros(8)
CL=np.zeros(8)
DH=np.zeros(8)
DL=np.zeros(8)
#wszystkie rejestry w jednej liscie
register =[AH, AL, BH, BL, CH, CL, DH, DL]

stos = np.zeros((10,8))

SP = [9]


#Funkcja zwracajaca indexy w liscie z rejestrami oraz ich dlugosc, na podstawie nazwy rejestru
def JakiRejestr(nazwa):
    index1=-1
    index2=-1
    rejestrDlugosc=0
    if len(nazwa)==1:
        rejestrDlugosc=16
        if nazwa == 'A':
            index1=0
            index2=1
        if nazwa == 'B':
            index1=2
            index2=3
        if nazwa == 'C':
            index1=4
            index2=5
        if nazwa == 'D':
            index1=6
            index2=7
    if len(nazwa)==2:
        rejestrDlugosc=8
        if nazwa == 'AH':
            index1=0
        if nazwa == 'AL':
            index1=1
        if nazwa == 'BH':
            index1=2
        if nazwa == 'BL':
            index1=3
        if nazwa == 'CH':
            index1=4
        if nazwa == 'CL':
            index1=5
        if nazwa == 'DH':
            index1=6
        if nazwa == 'DL':
            index1=7

    return rejestrDlugosc, index1, index2



#zamiana dziesietnej na binarna
def DigitToBin(lenth, date, register):
    for i in range(lenth):
        register[lenth-1-i]=int(date%2)
        date=int(date/2)

#ustawianie rejestrow w stan zerowy
def ZerowanieRejestrow():
    for i in range(8):
        for j in range(8):
            register[i][j]=0

    for i in range(10):
        for j in range(8):
            stos[i][j]=0
    print("Rejestry i stos Wyzerowano")

#wyswietlenie rejestrow w konsoli
def ShowRej():
    print(register[0]," ",register[1])
    print(register[2]," ",register[3])
    print(register[4]," ",register[5])
    print(register[6]," ",register[7])

#dodawanie rejestrowe
def ADDR(rejestrCel, rejestZrodlo):
    # do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)
    # z jakiego rejestru
    rejestrDlugosc1, index11, index22 = JakiRejestr(rejestZrodlo)
    #w tym rejestrze bedzie wynik
    acumulator = BinToDigit(index1, index2, rejestrDlugosc)
    print("oto acumulator: ", acumulator)
    #Z tego rejestru pobierane są dane
    dana = BinToDigit(index11, index22, rejestrDlugosc)
    #jesli liczba przekroczy dopuszczalny zakres to ma same jedynki
    if (acumulator + dana) > 65535:
        sum = 65535
        print('zaduzo')
    else:
        sum = acumulator + dana
    #tworzenie tymczasowego rejestru
    temp = np.zeros(16)
    DigitToBin(rejestrDlugosc, sum, temp)
    #w zaleznosci od tego czy jest to caly rejestr czy połówka
    if rejestrDlugosc == 8:
        register[index1] = temp[0:8]
    else:
        register[index1] = temp[0:8]
        register[index2] = temp[8:16]
    ShowRej()

#odejmowanie rejestrowe
def SUBR(rejestrCel, rejestZrodlo):
    # do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)

    # z jakiego rejestru
    rejestrDlugosc1, index11, index22 = JakiRejestr(rejestZrodlo)

    acumulator = BinToDigit(index1, index2, rejestrDlugosc)
    print("oto acumulator: ", acumulator)

    dana = BinToDigit(index11, index22, rejestrDlugosc)

    if (acumulator - dana) < 0:
        sum = 0
        print('zaduzo')
    else:
        sum = acumulator - dana

    temp = np.zeros(16)
    DigitToBin(rejestrDlugosc, sum, temp)

    if rejestrDlugosc == 8:
        register[index1] = temp[0:8]
    else:
        register[index1] = temp[0:8]
        register[index2] = temp[8:16]

    ShowRej()

#przenoszenie rejestrowe
def MOVR(rejestrCel, dana):
    # do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)

    # z jakiego rejestru
    rejestrDlugosc1, index11, index22 = JakiRejestr(dana)

    if rejestrDlugosc == rejestrDlugosc1:
        if rejestrDlugosc>8:
            register[index1] = register[index11]
            register[index2] = register[index22]
        else:
            register[index1] = register[index11]

    ShowRej()

#zamiana binarnej na dziesietną
def BinToDigit(regID1, regID2, lenth):
    date=0
    if lenth == 16:
        regA=register[regID1]
        regB=register[regID2]
        for i in range(int(lenth/2)):
            date = date + regB[int(lenth/2)-1-i]*2**i
        for i in range(int(lenth/2)):
            date = date + regA[int(lenth/2)-1-i]*2**(int(lenth/2)+i)
        return date
    else:
        regA = register[regID1]
        for i in range(int(lenth)):
            date = date + regA[int(lenth)-1-i]*2**i
        return date

#odejmowanie natychmiastowe
def SUB(rejestrCel, dana):
    sum=0

    # do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)

    acumulator = BinToDigit(index1, index2, rejestrDlugosc)
    print("oto acumulator: ",acumulator)

    if (acumulator-dana)< 0:
        sum=0
        print('zaduzo')
    else:
        sum = acumulator-dana

    temp = np.zeros(16)
    DigitToBin(rejestrDlugosc, sum, temp)

    if rejestrDlugosc == 8:
        register[index1] = temp[0:8]
    else:
        register[index1] = temp[0:8]
        register[index2] = temp[8:16]

    ShowRej()

#PUSH do stosu
def PUSH(rejestrCel):
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)

    for i in range(8):
        stos[SP[0]][i] = register[index1][i]
    SP[0]=SP[0]-1

def PULL(rejestrCel):
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)
    SP[0] = SP[0] + 1
    print("Jestem w rejestrze: ", rejestrCel,' o indeksie: ',index1, " jego zawartosc to: ",register[index1])
    print("licznik stosu = ", SP[0], ' stos w tym miejscu wynosi: ', stos[SP[0]])
    for i in range(8):
        register[index1][i] = stos[SP[0]][i]
        stos[SP[0]][i]=0


def przerwanie(rejestrCel):

    #rodzaj funkcji wywołanej w przerwaniu zależy od wartości w rejestrze AH
    rejestrDlugosc, index1, index2 = JakiRejestr('AH')
    numerFunkcji = BinToDigit(index1, index2, rejestrDlugosc)

    #pobierz czas
    if numerFunkcji==44:
        text=clock.czas
        print(text)
        texty=text.split(':')
        print(texty)
        textLiczba=texty[0]+texty[1]
        liczba=int(textLiczba)
        print(liczba)

        temp = np.zeros(16)
        DigitToBin(16, liczba, temp)

        register[index1] = temp[0:8]
        register[index1+1] = temp[8:16]

    # pobierz date
    if numerFunkcji == 42:
        data = kalendarz.data
        data1 = data.split('-')
        print(data1)
        textLiczba = data1[1] + data1[2]
        liczba = int(textLiczba)
        print(liczba)

        temp = np.zeros(16)
        DigitToBin(16, liczba, temp)

        register[index1] = temp[0:8]
        register[index1 + 1] = temp[8:16]

    # pobierz znak
    if numerFunkcji == 1:
        literka= input("Podaj znak: ")
        ASCI = [ord(x) for x in literka]
        print(ASCI)
        temp = np.zeros(8)
        DigitToBin(8, ASCI[0], temp)
        register[1] = temp

    # ustaw znak do schowka systemowego
    if numerFunkcji == 2:
        rejestrDlugosc, index1, index2 = JakiRejestr('AL')
        liczbaLitrka = BinToDigit(index1, index2, rejestrDlugosc)
        literka = chr(int(liczbaLitrka))
        pyperclip.copy(literka)

    # wyjscie z programu
    if numerFunkcji == 49:
        kalendarz.close()
        clock.close()
        ex.close()

    #ustawianie daty
    if numerFunkcji == 43:
        data='2018-04-'
        rejestrDlugosc, index1, index2 = JakiRejestr('AL')
        liczbaLitrka = BinToDigit(index1, index2, rejestrDlugosc)
        data=data+str(int(liczbaLitrka))
        print("TO SIEDZIALO W REJESTRZE: ",liczbaLitrka)
        kalendarz.showData(data)


    if numerFunkcji == 86:
        rejestrDlugosc, index1, index2 = JakiRejestr('AL')
        liczbaLitrka = BinToDigit(index1, index2, rejestrDlugosc)
        literka = chr(int(liczbaLitrka))
        slowo = 'foldery/'+str(literka) + '.txt'
        plik = open(slowo, 'w')
        plik.close()

    #usuwanie pliku
    if numerFunkcji == 65:

        rejestrDlugosc, index1, index2 = JakiRejestr('AL')
        liczbaLitrka = BinToDigit(index1, index2, rejestrDlugosc)
        literka = chr(int(liczbaLitrka))
        slowo = 'foldery/' + str(literka) + '.txt'

        if os.path.isfile(slowo):
            print()
            os.unlink(slowo)
        else:
            print("Wybacz, plik nie istnieje :(")

    # Pobieranie nazwy pliku
    if numerFunkcji == 60:
        path = "..\\cwiczenie1ASK\\foldery"
        print("Pliki w folderze: ")
        filenames = os.listdir(path)
        print(filenames)
        if filenames.__len__()>0:
            ASCI = [ord(x) for x in filenames[0]]
            print(ASCI)
            if ASCI[0]=='':
                temp = np.zeros(8)
                DigitToBin(8, ' ', temp)
                register[1] = temp
            else:
                temp = np.zeros(8)
                DigitToBin(8, ASCI[0], temp)
                register[1] = temp
        else:
            print("NIE MA PLIKOW")

        #Zmianna nazwy pliku
        if numerFunkcji == 61:
            path = "..\\cwiczenie1ASK\\foldery"
            print("Pliki w folderze: ")
            filenames = os.listdir(path)
            print(filenames)
            if filenames.__len__() > 0:
                slowo = 'foldery/' + filenames[0]
                nowaNazwa=input("Podaj nowa nazwe: ")
                os.rename(slowo, nowaNazwa)
            else:
                print("NIE MA PLIKOW")


#dodawanie natychmiastowe
def ADD(rejestrCel, dana):
    sum=0

    # do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)

    acumulator = BinToDigit(index1, index2, rejestrDlugosc)
    print("oto acumulator: ",acumulator)

    if (acumulator+dana)> 65535:
        sum=65535
        print('zaduzo')
    else:
        sum = acumulator+dana

    temp = np.zeros(16)
    DigitToBin(rejestrDlugosc, sum, temp)

    if rejestrDlugosc == 8:
        register[index1] = temp[0:8]
    else:
        register[index1] = temp[0:8]
        register[index2] = temp[8:16]

    ShowRej()

#dodawanie natychmiastowe
def MOV(rejestrCel, dana):
    #do jakiego rejestru wstawic dana
    rejestrDlugosc, index1, index2 = JakiRejestr(rejestrCel)
    print(rejestrCel, " oraz indexy: ",index1, index2)
    temp = np.zeros(16)
    DigitToBin(rejestrDlugosc, dana, temp)
    if rejestrDlugosc == 8:
        register[index1] = temp[0:8]
    else:
        register[index1] = temp[0:8]
        register[index2] = temp[8:16]

    ShowRej()



class Example(QWidget):
    def __init__(self):
        super().__init__()
        self.nazwaRozkazR=''
        self.nazwaRozkazN=''
        self.nazwaPrzerwania=''
        self.RejestrDocelowyR=''
        self.RejestrDocelowyN=''
        self.RejestrWybR=''
        self.danaN=''
        self.nazwaOperacji = ''
        self.RejestrOperacji = ''
        self.krok=0
        self.debug=False
        self.initUI()


    def initUI(self):

        grid = QGridLayout()
        self.setLayout(grid)

        positions = [(2,0), (2,1), (2,2), (1,0), (0,0), (3,1), (4,0), (5,0), (5,1), (5,2), (6,1), (10,0)]

        self.LabelDodajRozkaz = QLabel('Dodaj rozkaz: ')
        grid.addWidget(self.LabelDodajRozkaz, *positions[4])

        LabelTrybRejestrowy = QLabel('Tryb Rejestrowy: ')
        grid.addWidget(LabelTrybRejestrowy, *positions[3])

        self.rodzajRozkazuR = QComboBox(self)
        self.rodzajRozkazuR.addItem("MOVE")
        self.rodzajRozkazuR.addItem("ADD")
        self.rodzajRozkazuR.addItem("SUB")
        grid.addWidget(self.rodzajRozkazuR, *positions[0])

        self.rejstrWybranyR = QComboBox(self)
        self.rejstrWybranyR.addItem("A")
        self.rejstrWybranyR.addItem("B")
        self.rejstrWybranyR.addItem("C")
        self.rejstrWybranyR.addItem("D")
        self.rejstrWybranyR.addItem("AH")
        self.rejstrWybranyR.addItem("AL")
        self.rejstrWybranyR.addItem("BH")
        self.rejstrWybranyR.addItem("BL")
        self.rejstrWybranyR.addItem("CH")
        self.rejstrWybranyR.addItem("CL")
        self.rejstrWybranyR.addItem("DL")
        self.rejstrWybranyR.addItem("DH")
        grid.addWidget(self.rejstrWybranyR, *positions[1])

        self.rejstrWybrany1 = QComboBox(self)
        self.rejstrWybrany1.addItem("A")
        self.rejstrWybrany1.addItem("B")
        self.rejstrWybrany1.addItem("C")
        self.rejstrWybrany1.addItem("D")
        self.rejstrWybrany1.addItem("AH")
        self.rejstrWybrany1.addItem("AL")
        self.rejstrWybrany1.addItem("BH")
        self.rejstrWybrany1.addItem("BL")
        self.rejstrWybrany1.addItem("CH")
        self.rejstrWybrany1.addItem("CL")
        self.rejstrWybrany1.addItem("DL")
        self.rejstrWybrany1.addItem("DH")
        grid.addWidget(self.rejstrWybrany1, *positions[2])


        buttonDodajRejestrowo = QPushButton("DodajR")
        grid.addWidget(buttonDodajRejestrowo, *positions[5])
        buttonDodajRejestrowo.clicked.connect(self.on_click)

        LabelTrybNatychmiastowy = QLabel('Tryb Natychmiastowy: ')
        grid.addWidget(LabelTrybNatychmiastowy, *positions[6])

        self.rodzajRozkazuN = QComboBox(self)
        self.rodzajRozkazuN.addItem("MOVE")
        self.rodzajRozkazuN.addItem("ADD")
        self.rodzajRozkazuN.addItem("SUB")
        grid.addWidget(self.rodzajRozkazuN, *positions[7])

        self.rejstrWybranyN = QComboBox(self)
        self.rejstrWybranyN.addItem("A")
        self.rejstrWybranyN.addItem("B")
        self.rejstrWybranyN.addItem("C")
        self.rejstrWybranyN.addItem("D")
        self.rejstrWybranyN.addItem("AH")
        self.rejstrWybranyN.addItem("AL")
        self.rejstrWybranyN.addItem("BH")
        self.rejstrWybranyN.addItem("BL")
        self.rejstrWybranyN.addItem("CH")
        self.rejstrWybranyN.addItem("CL")
        self.rejstrWybranyN.addItem("DL")
        self.rejstrWybranyN.addItem("DH")
        grid.addWidget(self.rejstrWybranyN, *positions[8])

        self.danaNatychmiastowa = QLineEdit(self)
        grid.addWidget(self.danaNatychmiastowa, *positions[9])

        buttonDodajNatychmiastowo = QPushButton("DodajN")
        grid.addWidget(buttonDodajNatychmiastowo, *positions[10])
        buttonDodajNatychmiastowo.clicked.connect(self.on_click)

        LabelPrzerwanie = QLabel('Przerwanie: ')
        grid.addWidget(LabelPrzerwanie, *(7,0))

        self.rodzajPrzerwania = QComboBox(self)
        self.rodzajPrzerwania.addItem("INT 21h")
        grid.addWidget(self.rodzajPrzerwania, *(8,0))


        buttonDodajPrzerwanie = QPushButton("DodajP")
        grid.addWidget(buttonDodajPrzerwanie, *(9,0))
        buttonDodajPrzerwanie.clicked.connect(self.on_click)

        LabelTrybNatychmiastowy = QLabel('Operacje na Stosie: ')
        grid.addWidget(LabelTrybNatychmiastowy, *(10,0))

        self.RodzajOperacji = QComboBox(self)
        self.RodzajOperacji.addItem("PUSH")
        self.RodzajOperacji.addItem("PULL")
        grid.addWidget(self.RodzajOperacji, *(11,0))

        self.OperacjaRejestr = QComboBox(self)
        self.OperacjaRejestr.addItem("AH")
        self.OperacjaRejestr.addItem("AL")
        self.OperacjaRejestr.addItem("BH")
        self.OperacjaRejestr.addItem("BL")
        self.OperacjaRejestr.addItem("CH")
        self.OperacjaRejestr.addItem("CL")
        self.OperacjaRejestr.addItem("DL")
        self.OperacjaRejestr.addItem("DH")
        grid.addWidget(self.OperacjaRejestr, *(11,1))

        buttonDodajOperacje = QPushButton("DodajO")
        grid.addWidget(buttonDodajOperacje, *(12,0))
        buttonDodajOperacje.clicked.connect(self.on_click)

        buttonWykonaj = QPushButton("Wykonaj")
        grid.addWidget(buttonWykonaj, *(7,2))
        buttonWykonaj.clicked.connect(self.on_click)

        buttonWykonajKrok = QPushButton("WykonajKrok")
        grid.addWidget(buttonWykonajKrok, *(8, 3))
        buttonWykonajKrok.clicked.connect(self.on_click)

        buttonDebuguj = QPushButton("Debuguj/Stop")
        grid.addWidget(buttonDebuguj, *(9, 3))
        buttonDebuguj.clicked.connect(self.on_click)

        self.Labeldebug = QLabel('Nie Debugujesz ')
        grid.addWidget(self.Labeldebug, *(9, 2))

        buttonWgraj = QPushButton("Wgraj")
        grid.addWidget(buttonWgraj, *(7, 3))
        buttonWgraj.clicked.connect(self.on_click)

        buttonZapisz = QPushButton("Zapisz")
        grid.addWidget(buttonZapisz, *(8, 2))
        buttonZapisz.clicked.connect(self.on_click)

        self.RejestryLabels=[]
        for i in range(9): #utworzenie osmiu label reprezentujacych rejestry oraz ostatnia zawartosc stosu
            temp=[]
            for j in range(8):
                temp.append(QLabel('0'))
                positions.append((i, j + 5))
                grid.addWidget(temp[j], *positions[12 + i*8+j])
            self.RejestryLabels.append(temp)




        LabelAH = QLabel('Rejestr AH: ')
        grid.addWidget(LabelAH, *(0,4))
        LabelAL = QLabel('Rejestr AL: ')
        grid.addWidget(LabelAL, *(1, 4))
        LabelBH = QLabel('Rejestr BH: ')
        grid.addWidget(LabelBH, *(2, 4))
        LabelBL = QLabel('Rejestr BL: ')
        grid.addWidget(LabelBL, *(3, 4))
        LabelCH = QLabel('Rejestr CH: ')
        grid.addWidget(LabelCH, *(4, 4))
        LabelCL = QLabel('Rejestr CL: ')
        grid.addWidget(LabelCL, *(5, 4))
        LabelDH = QLabel('Rejestr DH: ')
        grid.addWidget(LabelDH, *(6, 4))
        LabelDL = QLabel('Rejestr DL: ')
        grid.addWidget(LabelDL, *(7, 4))

        LabelStos = QLabel('Stos: ')
        grid.addWidget(LabelStos, *(8, 4))

        LabelStosPointer = QLabel('Stos Pointer: ')
        grid.addWidget(LabelStosPointer, *(9, 4))

        self.LabelStosPointerValue = QLabel('9')
        grid.addWidget(self.LabelStosPointerValue, *(9, 5))




        self.PoleTekstowe = QPlainTextEdit(self)
        self.PoleTekstowe.insertPlainText("")
        self.PoleTekstowe.ensureCursorVisible()
        self.PoleTekstowe.setReadOnly(True)
        grid.addWidget(self.PoleTekstowe, *(13,3))

        # self.zegar= DigitalClock()
        # grid.addWidget(self.zegar, *(15, 3))

        self.move(300, 150)
        self.setWindowTitle('Procesorek')
        self.show()


    #@pyqtSlot()
    def on_click(self):
        sender = self.sender()
        if sender.text()=='DodajR':
            self.nazwaRozkazR= self.rodzajRozkazuR.currentText()
            self.RejestrDocelowyR = self.rejstrWybranyR.currentText()
            self.RejestrWybR = self.rejstrWybrany1.currentText()
            if len(self.RejestrDocelowyR)== len(self.RejestrWybR):
                poleTekstoweZawartosc = self.nazwaRozkazR + " " + self.RejestrDocelowyR + " " + self.RejestrWybR + "\n"
                #print(poleTekstoweZawartosc)
                self.PoleTekstowe.insertPlainText(poleTekstoweZawartosc)
                #print(self.nazwaRozkazR, " ", self.RejestrDocelowyR, ", ", self.RejestrWybR)
            else:
                buttonReply = QMessageBox.question(self, 'ERROR', "Wybierz rejestry o tej samej pojemnosci",
                                                   QMessageBox.Yes)

        if sender.text()=='DodajO':
            self.nazwaOperacji= self.RodzajOperacji.currentText()
            self.RejestrOperacji = self.OperacjaRejestr.currentText()
            poleTekstoweZawartosc = self.nazwaOperacji + " " + self.RejestrOperacji + " " + "\n"
            #print(poleTekstoweZawartosc)
            self.PoleTekstowe.insertPlainText(poleTekstoweZawartosc)
            #print(self.nazwaRozkazR, " ", self.RejestrDocelowyR, ", ", self.RejestrWybR)


        if sender.text()=='DodajP':
            self.nazwaPrzerwania= self.rodzajPrzerwania.currentText()
            poleTekstoweZawartosc = self.nazwaPrzerwania + "\n"
            #print(poleTekstoweZawartosc)
            self.PoleTekstowe.insertPlainText(poleTekstoweZawartosc)
            #print(self.nazwaRozkazR, " ", self.RejestrDocelowyR, ", ", self.RejestrWybR)


        if sender.text()=='DodajN':
            self.nazwaRozkazN= self.rodzajRozkazuN.currentText()
            self.RejestrDocelowyN = self.rejstrWybranyN.currentText()
            self.DanaN = self.danaNatychmiastowa.text()

            try:
                liczba = int(self.DanaN)

                if (len(self.RejestrDocelowyN) == 2 and liczba <= 255) or (len(self.RejestrDocelowyN) == 1 and liczba <= 65535):
                    poleTekstoweZawartosc = self.nazwaRozkazN + " " + self.RejestrDocelowyN + " " + self.DanaN + "\n"
                    #print(poleTekstoweZawartosc)
                    self.PoleTekstowe.insertPlainText(poleTekstoweZawartosc)
                    #print(self.nazwaRozkazN, " ", self.RejestrDocelowyN, ", ", self.DanaN)
                else:
                   # self.PoleTekstowe.insertPlainText("kurwa za duzo\n")
                    buttonReply = QMessageBox.question(self, 'ERROR', "Za duza liczba",
                                                       QMessageBox.Yes)
            except:
                buttonReply = QMessageBox.question(self, 'ERROR', "Mogą być tylko liczby",
                                                   QMessageBox.Yes)

#TODO KROKOWO działa średni, trzeba poprawic dla funkcji PUSH i PULL

        if sender.text() == 'Debuguj/Stop':
            if self.debug:
                self.debug=False
                self.Labeldebug.setText('Nie Debugujesz')
            else:
                self.debug=True
                nazwa = "Debugujesz, Krok: " + str(self.krok)
                self.Labeldebug.setText(nazwa)
                ZerowanieRejestrow()
                SP[0] = 9
            self.krok=0



        if sender.text() == 'WykonajKrok':
            if self.debug:
                self.plik = open('dane.txt', 'r')
                text = self.plik.readlines()
                print(text)
                wykonalKrok=False
                licznik = 0
                for line in text:
                    if licznik != self.krok or wykonalKrok:
                        #print('niespelniony: oto krok i licznik: ', self.krok, licznik)
                        licznik=licznik+1

                    else:
                        #print('oto krok i licznik: ', self.krok, licznik)
                        wykonalKrok = True
                        #print(line)
                        if len(line) > 3:
                            words = line.split()

                            print(words)
                            if words.__len__() == 1:
                                rozkaz = words
                            if words.__len__() == 2:
                                rozkaz, rejestrCel = words
                            if words.__len__() == 3:
                                rozkaz, rejestrCel, dana = words
                            try:
                                liczba = int(dana)
                                trybNat = True
                            except:
                                print("nie udalo sie, dana")
                                trybNat = False
                                pass

                            if rozkaz == 'MOVE':
                                print("tu sie zaczyna wykonywać MOV")

                                if trybNat:
                                    MOV(rejestrCel, liczba)
                                else:
                                    MOVR(rejestrCel, dana)
                            if rozkaz == 'ADD':
                                print("tu sie zaczyna wykonywać Add")

                                if trybNat:
                                    ADD(rejestrCel, liczba)
                                else:
                                    ADDR(rejestrCel, dana)
                            if rozkaz == 'SUB':
                                print("tu sie zaczyna wykonywać Sub")

                                if trybNat:
                                    SUB(rejestrCel, liczba)
                                else:
                                    SUBR(rejestrCel, dana)
                            if rozkaz == 'PUSH':
                                if SP[0] > 0:
                                    PUSH(rejestrCel)
                                    print("stos, SP", stos, SP)
                                    # self.LabelStosPointerValue.setText('K')
                                    for i in range(8):
                                        self.RejestryLabels[8][i].setText(str(int(stos[SP[0] + 1][i])))
                                    self.LabelStosPointerValue.setText(str(int(SP[0] + 1)))
                                else:
                                    buttonReply = QMessageBox.question(self, 'ERROR', "Stos jest już pełny",
                                                                       QMessageBox.Yes)
                            if rozkaz == 'PULL':
                                print("pull me")
                                if SP[0] < 9:
                                    PULL(rejestrCel)
                                    print("stos, SP", stos, SP)
                                    # self.LabelStosPointerValue.setText('K')
                                    for i in range(8):
                                        self.RejestryLabels[8][i].setText(str(int(stos[SP[0] - 1][i])))
                                    self.LabelStosPointerValue.setText(str(int(SP[0] - 1)))
                                else:
                                    buttonReply = QMessageBox.question(self, 'ERROR', "Stos jest już pusty",
                                                                       QMessageBox.Yes)
                            if rozkaz == 'INT':
                                przerwanie(rejestrCel)
                                for i in range(8):
                                    self.RejestryLabels[8][i].setText(str(int(stos[SP[0]][i])))
                                self.LabelStosPointerValue.setText(str(int(SP[0])))
                                print("A tu będzie ciekawie")


                for i in range(8):
                    for j in range(8):
                        self.RejestryLabels[i][j].setText(str(int(register[i][j])))
                        # RejestryLabels.append(temp)
                self.krok = self.krok + 1
                nazwa = "Debugujesz, Krok: " + str(self.krok)
                self.Labeldebug.setText(nazwa)
                self.plik.close()

        if sender.text() == 'Wykonaj':
            self.plik = open('dane.txt', 'r')
            ZerowanieRejestrow()
            text = self.plik.readlines()
            print(text)
            #self.PoleTekstowe.setPlainText(text)
            SP[0]=9
            for line in text:
                if len(line) > 3:
                    words = line.split()

                    print(words)
                    if words.__len__()==1:
                        rozkaz = words
                    if words.__len__()==2:
                        rozkaz, rejestrCel = words
                    if words.__len__() == 3:
                        rozkaz, rejestrCel, dana = words
                    try:
                        liczba = int(dana)
                        trybNat=True
                    except:
                        print("nie udalo sie, dana")
                        trybNat = False
                        pass

                    if rozkaz=='MOVE':
                        print("tu sie zaczyna wykonywać MOV")

                        if trybNat:
                            MOV(rejestrCel,liczba)
                        else:
                            MOVR(rejestrCel, dana)
                    if rozkaz=='ADD':
                        print("tu sie zaczyna wykonywać Add")

                        if trybNat:
                            ADD(rejestrCel,liczba)
                        else:
                            ADDR(rejestrCel, dana)
                    if rozkaz=='SUB':
                        print("tu sie zaczyna wykonywać Sub")

                        if trybNat:
                            SUB(rejestrCel,liczba)
                        else:
                            SUBR(rejestrCel, dana)
                    ##TODO całość zwiazana z przerwaniami
                    if rozkaz == 'PUSH':
                        if SP[0]>0:
                            PUSH(rejestrCel)
                            print("stos, SP", stos, SP)
                            #self.LabelStosPointerValue.setText('K')
                            for i in range(8):
                                self.RejestryLabels[8][i].setText(str(int(stos[SP[0]+1][i])))
                            self.LabelStosPointerValue.setText(str(int(SP[0] + 1)))
                        else:
                            buttonReply = QMessageBox.question(self, 'ERROR', "Stos jest już pełny",
                                                               QMessageBox.Yes)
                    if rozkaz == 'PULL':
                        print("pull me")
                        if SP[0] < 9:
                            PULL(rejestrCel)
                            print("stos, SP", stos, SP)
                            # self.LabelStosPointerValue.setText('K')
                            for i in range(8):
                                self.RejestryLabels[8][i].setText(str(int(stos[SP[0] - 1][i])))
                            self.LabelStosPointerValue.setText(str(int(SP[0] - 1)))
                        else:
                            buttonReply = QMessageBox.question(self, 'ERROR', "Stos jest już pusty",
                                                               QMessageBox.Yes)
                    if rozkaz == 'INT':
                        przerwanie(rejestrCel)
                        for i in range(8):
                            self.RejestryLabels[8][i].setText(str(int(stos[SP[0]][i])))
                        self.LabelStosPointerValue.setText(str(int(SP[0])))
                        print("A tu będzie ciekawie")

            for i in range(8):
                for j in range(8):
                    self.RejestryLabels[i][j].setText(str(int(register[i][j])))



                #RejestryLabels.append(temp)



            #TODO tutajdodac wyswietlane ostatniej rzeczy na stosie oraz licznika sp
            self.plik.close()


        if sender.text() == 'Wgraj':
            self.plik = open('dane.txt')
            text = self.plik.read()
            #print(text)
            self.PoleTekstowe.setPlainText(text)
            self.plik.close()

        if sender.text() == 'Zapisz':
            plik = open('dane.txt', 'w')
            plik.write("")
            plik.close()

            self.plik = open('dane.txt', 'a')
            self.PoleTekstowe.selectAll()
            self.PoleTekstowe.copy()
            win32c.OpenClipboard()
            data = win32c.GetClipboardData()
            win32c.CloseClipboard()
            print(data)

            for linia in data:
                if linia !='\n':
                    self.plik.write(linia)
            self.plik.close()





if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    clock = DigitalClock()
    clock.show()
    kalendarz = DigitalData()
    kalendarz.show()
    sys.exit(app.exec_())
