import sys
import os
from threading import Thread
from time import sleep
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QFont
from PyQt5.QtCore import pyqtSignal, pyqtSlot
import rs232
from profanity_filter import ProfanityFilter
from receiverui import Ui_Odbiornik


class Receiver(QMainWindow, Ui_Odbiornik):
    data_pending = pyqtSignal()

    def __init__(self):
        super().__init__()

        self.filter = ProfanityFilter()

        # SETUP GUI
        self.setupUi(self)
        self.setFixedSize(670, 450)
        # Setup font
        font = QFont()
        font.setFamily('courier')
        self.rx_history.setFont(font)
        self.rx_bin.setFont(font)

        self.show()

        self.listener = Thread(target=self.rs_listen)
        self.data_pending.connect(self.on_received_data)
        self.listener.start()

    def rs_listen(self):
        while True:
            if os.path.isfile('medium.txt'):
                if os.path.getsize('medium.txt') > 0:
                    self.data_pending.emit()
            sleep(0.1)

    def on_received_data(self):
        try:
            medium = open('medium.txt', 'r+')
            encoded_message = medium.read()
            medium.truncate(0)
            decoded_message = rs232.decode_string(encoded_message)
            censored_message = self.filter.censor(decoded_message)
            self.rx_bin.setPlainText(encoded_message)
            self.rx_history.append(censored_message[:-1])
        except Exception as e:
            print(e)


rec = QApplication(sys.argv)
h = Receiver()
sys.exit(rec.exec_())
