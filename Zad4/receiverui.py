# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'receiverui.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Odbiornik(object):
    def setupUi(self, Odbiornik):
        Odbiornik.setObjectName("Odbiornik")
        Odbiornik.resize(690, 460)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Odbiornik.sizePolicy().hasHeightForWidth())
        Odbiornik.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(Odbiornik)
        self.centralwidget.setObjectName("centralwidget")
        self.rx_history = QtWidgets.QTextBrowser(self.centralwidget)
        self.rx_history.setGeometry(QtCore.QRect(20, 30, 631, 201))
        self.rx_history.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.rx_history.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.rx_history.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.rx_history.setObjectName("rx_history")
        self.rx_bin = QtWidgets.QTextBrowser(self.centralwidget)
        self.rx_bin.setGeometry(QtCore.QRect(20, 260, 631, 145))
        self.rx_bin.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.rx_bin.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.rx_bin.setObjectName("rx_bin")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 47, 13))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 240, 81, 16))
        self.label_2.setObjectName("label_2")
        Odbiornik.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Odbiornik)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 690, 21))
        self.menubar.setObjectName("menubar")
        Odbiornik.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Odbiornik)
        self.statusbar.setObjectName("statusbar")
        Odbiornik.setStatusBar(self.statusbar)

        self.retranslateUi(Odbiornik)
        QtCore.QMetaObject.connectSlotsByName(Odbiornik)

    def retranslateUi(self, Odbiornik):
        _translate = QtCore.QCoreApplication.translate
        Odbiornik.setWindowTitle(_translate("Odbiornik", "Odbiornik"))
        self.label.setText(_translate("Odbiornik", "Historia"))
        self.label_2.setText(_translate("Odbiornik", "Postać binarna"))

