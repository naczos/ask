# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'transceiverui.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Nadajnik(object):
    def setupUi(self, Nadajnik):
        Nadajnik.setObjectName("Nadajnik")
        Nadajnik.resize(670, 505)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Nadajnik.sizePolicy().hasHeightForWidth())
        Nadajnik.setSizePolicy(sizePolicy)
        self.centralwidget = QtWidgets.QWidget(Nadajnik)
        self.centralwidget.setObjectName("centralwidget")
        self.tx_history = QtWidgets.QTextBrowser(self.centralwidget)
        self.tx_history.setGeometry(QtCore.QRect(20, 30, 631, 201))
        self.tx_history.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tx_history.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tx_history.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.tx_history.setObjectName("tx_history")
        self.tx_bin = QtWidgets.QTextBrowser(self.centralwidget)
        self.tx_bin.setGeometry(QtCore.QRect(20, 310, 631, 145))
        self.tx_bin.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tx_bin.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.tx_bin.setObjectName("tx_bin")
        self.tx_input = QtWidgets.QLineEdit(self.centralwidget)
        self.tx_input.setGeometry(QtCore.QRect(20, 260, 631, 20))
        self.tx_input.setMaxLength(60)
        self.tx_input.setEchoMode(QtWidgets.QLineEdit.Normal)
        self.tx_input.setClearButtonEnabled(False)
        self.tx_input.setObjectName("tx_input")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 290, 71, 16))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(20, 240, 61, 16))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(20, 10, 47, 13))
        self.label_3.setObjectName("label_3")
        Nadajnik.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(Nadajnik)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 670, 21))
        self.menubar.setObjectName("menubar")
        Nadajnik.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(Nadajnik)
        self.statusbar.setObjectName("statusbar")
        Nadajnik.setStatusBar(self.statusbar)

        self.retranslateUi(Nadajnik)
        QtCore.QMetaObject.connectSlotsByName(Nadajnik)

    def retranslateUi(self, Nadajnik):
        _translate = QtCore.QCoreApplication.translate
        Nadajnik.setWindowTitle(_translate("Nadajnik", "Nadajnik"))
        self.label.setText(_translate("Nadajnik", "Postać binarna"))
        self.label_2.setText(_translate("Nadajnik", "Wiadomość"))
        self.label_3.setText(_translate("Nadajnik", "Historia"))

