import re


class ProfanityFilter:
    def __init__(self):
        self.profanities = set(line.strip() for line in open('profanities.txt'))

    @staticmethod
    def replacer(match):
        profanity = match.group()
        return '*' * len(profanity)

    def censor(self, text: str):
        regexp = (r'(%s)' % '|'.join(self.profanities))
        r = re.compile(regexp, re.IGNORECASE)
        return r.sub(self.replacer, text)
