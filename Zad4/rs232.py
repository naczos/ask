def encode_char(char: chr):
    start_bit = '0'
    stop_bits = '11'
    data = bin(ord(char))[2:].zfill(8)          # Convert char to binary string
    data = data[::-1]                           # Reverse data
    return start_bit + data + stop_bits


def decode_frame(frame: str):
    data = frame[1:-2]                          # Rip off start and stop bits
    data = data[::-1]                           # Reverse data
    ascii_code = int(data, base=2)
    return chr(ascii_code)


def encode_string(string: str):
    encoded_string = ''
    for char in string:
        encoded_string += encode_char(char)
    return encoded_string


def decode_string(string: str):
    decoded_string = ''
    frames = [string[i:i+11] for i in range(0, len(string), 11)]        # Split string into 11-bit frames
    for frame in frames:
        decoded_string += decode_frame(frame)
    return decoded_string
