import sys
from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtGui import QFont
import rs232
from profanity_filter import ProfanityFilter
from transceiverui import Ui_Nadajnik


class Transceiver(QMainWindow, Ui_Nadajnik):
    MESSAGE_MAX_LENGTH = 76

    def __init__(self):
        super().__init__()

        self.filter = ProfanityFilter()

        # SETUP GUI
        self.setupUi(self)
        self.setFixedSize(670, 500)
        self.tx_input.setMaxLength(self.MESSAGE_MAX_LENGTH)
        # Setup font
        font = QFont()
        font.setFamily('courier')
        self.tx_input.setFont(font)
        self.tx_history.setFont(font)
        self.tx_bin.setFont(font)

        self.show()

        self.tx_input.returnPressed.connect(self.on_send)

    def on_send(self):
        message = self.tx_input.text() + '\n'
        encoded_message = rs232.encode_string(message)

        self.write_history(message[:-1])
        self.tx_bin.setText(encoded_message)

        try:
            with open('medium.txt', 'a+') as medium:
                medium.write(encoded_message)
        except Exception as e:
            print(e)

        self.tx_input.clear()

    def write_history(self, message: str):
        message = self.filter.censor(message)
        self.tx_history.append(message)


tra = QApplication(sys.argv)
h = Transceiver()
sys.exit(tra.exec_())
