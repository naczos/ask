# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'form.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(600, 143)
        self.passwordInput = QtWidgets.QLineEdit(Form)
        self.passwordInput.setGeometry(QtCore.QRect(192, 50, 161, 20))
        self.passwordInput.setObjectName("passwordInput")
        self.fanBox = QtWidgets.QCheckBox(Form)
        self.fanBox.setGeometry(QtCore.QRect(20, 90, 131, 17))
        self.fanBox.setObjectName("fanBox")
        self.passwordLabel = QtWidgets.QLabel(Form)
        self.passwordLabel.setGeometry(QtCore.QRect(190, 20, 151, 16))
        self.passwordLabel.setObjectName("passwordLabel")
        self.statusLabel = QtWidgets.QLabel(Form)
        self.statusLabel.setGeometry(QtCore.QRect(20, 120, 571, 16))
        self.statusLabel.setObjectName("statusLabel")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.fanBox.setText(_translate("Form", "Dodatkowy wentylator"))
        self.passwordLabel.setText(_translate("Form", "Wprowadź hasło"))
        self.statusLabel.setText(_translate("Form", "Wszystko OK."))

