import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from PyQt5.QtWidgets import QWidget, QApplication, QMessageBox
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtCore import Qt

from form import Ui_Form


class Simulator(QWidget, Ui_Form):
    def __init__(self):
        super().__init__()

        self.setupUi(self)
        self.fanBox.setVisible(False)
        self.statusLabel.setVisible(False)
        self.passwordInput.returnPressed.connect(self.on_login)

        self.fanBox.setDisabled(True)

        self.popup_timer = QTimer()
        self.popup_timer.setSingleShot(True)
        self.popup_timer.timeout.connect(self.popup)

        self.logout_timer = QTimer()
        self.logout_timer.setSingleShot(True)
        self.logout_timer.timeout.connect(self.on_logout)

        self.ani = None
        self.show()

    def keyPressEvent(self, e: QKeyEvent):
        print('wcis')
        if e.key() == Qt.Key_Return:
            self.trend = 0
            self.statusLabel.setText('Wszystko OK.')


    def on_login(self):
        text = self.passwordInput.text()
        self.passwordInput.clear()
        if text == 'admin':
            self.passwordInput.setVisible(False)
            self.passwordLabel.setVisible(False)

            self.fanBox.setVisible(True)
            self.statusLabel.setVisible(True)
            self.popup_timer.start(30000)
            if self.ani:
                self.fig.set_visible(True)
            else:
                self.start_animation()


    def on_logout(self):
        self.fig.set_visible(False)
        self.fanBox.setVisible(False)
        self.passwordLabel.setVisible(True)
        self.passwordInput.setVisible(True)
        self.msg.setText("Użytkownik wylogowany z powodu bezczynności")

    def popup(self):
        self.msg = QMessageBox()
        self.msg.setIcon(QMessageBox.Information)
        self.msg.setText("Potwierdź swoją obecność")
        self.msg.setWindowTitle("Alive")
        self.msg.setStandardButtons(QMessageBox.Ok)
        self.msg.buttonClicked.connect(self.alive)
        self.msg.show()
        self.logout_timer.start(10000)

    def alive(self):
        self.logout_timer.stop()
        self.popup_timer.start(30000)

    def start_animation(self):
        self.fan = 0
        self.x = np.linspace(-60, 0, 1200)
        self.mean = 50
        self.trend = 0
        self.y = [self.mean]

        mpl.rcParams['toolbar'] = 'None'
        self.fig = plt.figure()
        ax = self.fig.add_subplot(111)
        ax.set_xlim(-60, 0)
        ax.set_ylim(30, 90)
        ax.set_xlabel('Czas [s]')
        ax.set_ylabel('Temperatura [°C]')
        self.line, = ax.plot(self.x[-1], self.y)
        self.line_up_lim, = ax.plot([-60, 0], [80, 80])
        self.line_lo_lim, = ax.plot([-60, 0], [60, 60])

        self.ani = animation.FuncAnimation(self.fig, self.update, interval=50)
        plt.show()

    def update(self, _):
        if self.y[-1] > 80:
            self.fan = 0.2
            self.fanBox.setChecked(True)
        elif self.y[-1] < 60:
            self.fan = 0
            self.fanBox.setChecked(False)
        if np.random.randint(0, 501) > 499:
            self.trend = 0.15
            self.statusLabel.setText('Awaria układu chłodzenia!!! (Naciśnij enter aby naprawić)')

        self.y.append(self.y[-1] + np.random.randint(-100, 105)/200 - self.fan + self.trend)
        if len(self.y) > 1200:
            self.y = self.y[1:]
        self.line.set_xdata(self.x[1200 - len(self.y):])
        self.line.set_ydata(self.y)


tra = QApplication(sys.argv)
h = Simulator()
sys.exit(tra.exec_())
