#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from PyQt5.QtWidgets import QCheckBox, QButtonGroup, QVBoxLayout, QHBoxLayout, QGridLayout, QLabel, QPushButton, QMessageBox
from PyQt5.QtCore import Qt
import random
import datetime
import time
import winsound
import matplotlib.pyplot as plt
import numpy as np



class Ui_Widget(object):
    """ Klasa definiująca GUI """

    def setupUi(self, Widget):
        self.left = 50
        self.top = 50
        self.width = 600
        self.height = 500
        self.buttonHeight=250
        self.buttonWidth=300
        self.liczba=0
        self.LicznikRozgrzewki=4
        self.LicznikTest=10
        self.start = datetime.datetime.now()
        self.stanGry = 0 # 0 -> czekaj, 1 -> wizualizacja rozgrzewka, 2-> wizualizacja test, 3-> audio rozgrzewka, 4-> audio test
        self.WynikiWizualizacja=[]
        self.WynikiAudio=[]
        self.tekstPolecenia = "Witam na tescie psychomotorycznym \nMenu: \n*Naciśnij Q aby rozpoczac rogrzewke do czesci wizualnej\n*Naciśnij W aby rozpoczac test z czesci wizualnej\n*Naciśnij E aby rozpoczac rozgrzewke do czesci Audio\n*Naciśnij R aby rozpoczac test z czesci Audio"
        self.tekstWynik = "W części wizualnej należy\njak najszybciej kliknąć na czerwony kafelek\nW części Audi należy po usłyszeniu\ndźwieku jak najszybciej kliknąć na dowolny kafelek\n\nOto wynik czesciowy: "

        self.polecenie = QLabel(self.tekstPolecenia , self)
        self.wynik = QLabel(self.tekstWynik, self)

        self.polecenie.setMaximumWidth(300)

        ukladT = QGridLayout()

        self.Btn1 = QPushButton("1", self)
        self.Btn2 = QPushButton("2", self)
        self.Btn3 = QPushButton("3", self)
        self.Btn4 = QPushButton("4", self)


        ukladT.addWidget(self.polecenie, 0,0)
        ukladT.addWidget(self.wynik, 0,1)
        ukladT.addWidget(self.Btn1, 1,0)
        ukladT.addWidget(self.Btn2, 1,1)
        ukladT.addWidget(self.Btn3, 2,0)
        ukladT.addWidget(self.Btn4, 2,1)

        self.Btn1.setMinimumHeight(self.buttonHeight)
        self.Btn2.setMinimumHeight(self.buttonHeight)
        self.Btn3.setMinimumHeight(self.buttonHeight)
        self.Btn4.setMinimumHeight(self.buttonHeight)

        self.Btn1.setMinimumWidth(self.buttonWidth)
        self.Btn2.setMinimumWidth(self.buttonWidth)
        self.Btn3.setMinimumWidth(self.buttonWidth)
        self.Btn4.setMinimumWidth(self.buttonWidth)

        self.Btn1.clicked.connect(self.odpowiedz)
        self.Btn2.clicked.connect(self.odpowiedz)
        self.Btn3.clicked.connect(self.odpowiedz)
        self.Btn4.clicked.connect(self.odpowiedz)

        self.wygasaniePol()

        # przypisanie utworzonego układu do okna
        self.setLayout(ukladT)

        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setWindowTitle("Prosty test psychomotoryczny")
        self.show()


    def wygasaniePol(self):
        self.Btn1.setStyleSheet("background-color: blue")
        self.Btn2.setStyleSheet("background-color: blue")
        self.Btn3.setStyleSheet("background-color: blue")
        self.Btn4.setStyleSheet("background-color: blue")

    def odpowiedz(self):
        nadawca = self.sender()
        if self.stanGry == 1:
            if self.LicznikRozgrzewki > 0:
                if int(nadawca.text()) == self.liczba:
                    self.LicznikRozgrzewki-=1
                    duration = datetime.datetime.now() - self.start
                    self.wynik.setText(self.tekstWynik+str(duration.seconds)+":"+str(duration.microseconds))
                    self.Test()
            else:
                self.stanGry=0
                self.wygasaniePol()


        elif self.stanGry==2:
            if self.LicznikTest>0:
                if int(nadawca.text())==self.liczba:
                    duration = datetime.datetime.now() - self.start
                    self.wynik.setText(self.tekstWynik+str(duration.seconds) + ":" + str(duration.microseconds))
                    self.WynikiWizualizacja.append(duration)
                    self.Test()
                    self.LicznikTest -= 1
            else:
                self.stanGry=0
                self.wygasaniePol()
                self.liczSrednia(self.WynikiWizualizacja)



        elif self.stanGry==3:
            if self.LicznikRozgrzewki > 0:
                self.LicznikRozgrzewki -= 1
                duration = datetime.datetime.now() - self.start
                self.wynik.setText(self.tekstWynik+str(duration.seconds) + ":" + str(duration.microseconds))
                self.dzwiek()
            else:
                self.stanGry=0


        elif self.stanGry==4:
            if self.LicznikTest > 0:
                self.LicznikTest -= 1
                duration = datetime.datetime.now() - self.start
                self.wynik.setText(self.tekstWynik+str(duration.seconds) + ":" + str(duration.microseconds))
                self.WynikiAudio.append(duration)
                self.dzwiek()
            else:
                self.stanGry=0
                self.liczSrednia(self.WynikiAudio)

    def liczSrednia(self, listaWynikow):
        sumaMicroseconds=0
        ListaWynikWmicroseconds = []
        for czas in listaWynikow:
            wynikWmicroseconds = czas.seconds*1000000 + czas.microseconds
            ListaWynikWmicroseconds.append(wynikWmicroseconds)
            sumaMicroseconds +=wynikWmicroseconds

        sredniaMicroseconds = int(sumaMicroseconds/listaWynikow.__len__())
        srdeniaSeconds = int(sredniaMicroseconds/1000000)
        sredniaMicroseconds -= srdeniaSeconds
        wynik = str(srdeniaSeconds)+'s i '+str(sredniaMicroseconds)+"us"
        QMessageBox.about(self, "Wynik", "Oto średnia z uzyskanych wyników\n"+wynik)

        t = np.arange(1, listaWynikow.__len__()+1, 1)
        s = np.asarray(ListaWynikWmicroseconds)

        plt.plot(t, s, 'bo')

        plt.xlabel('Próbka [n]')
        plt.ylabel('time (us)')
        plt.title('Wyniki czastkowe')
        plt.grid(True)

        plt.show()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Q:
            self.stanGry=1
            self.LicznikRozgrzewki = 5
            time.sleep(3)
            self.Test()

        if e.key() == Qt.Key_W:
            self.stanGry=2
            self.WynikiWizualizacja=[]
            self.LicznikTest=9
            time.sleep(3)
            self.Test()

        if e.key() == Qt.Key_E:
            self.stanGry = 3
            self.LicznikRozgrzewki = 5
            time.sleep(2)
            self.dzwiek()

        if e.key() == Qt.Key_R:
            self.stanGry = 4
            self.WynikiAudio=[]
            self.LicznikTest = 9
            time.sleep(2)
            self.dzwiek()

    def dzwiek(self):
        losowyCzas = random.randint(500, 2000)
        time.sleep(losowyCzas/1000)
        self.start = datetime.datetime.now()
        winsound.PlaySound('beep.wav', winsound.SND_FILENAME)


    def Test(self):
        poprzednia =self.liczba
        while(True):
            self.liczba = random.randint(1, 4)
            if poprzednia == self.liczba:
                continue
            self.wygasaniePol()
            self.start = datetime.datetime.now()
            if self.liczba == 1:
                self.Btn1.setStyleSheet("background-color: red")
            elif self.liczba == 2:
                self.Btn2.setStyleSheet("background-color: red")
            elif self.liczba == 3:
                self.Btn3.setStyleSheet("background-color: red")
            else:
                self.Btn4.setStyleSheet("background-color: red")
            break
