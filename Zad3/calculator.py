import sys
import re
import math
from functools import partial
from pynput.keyboard import Listener
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette, QColor, QBrush
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, QAction, QDialog, QDialogButtonBox
from mainwindow import Ui_MainWindow
from dialog import Ui_Dialog


class BrightnessDialogBox(QDialog, Ui_Dialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)


class Calculator(QMainWindow, Ui_MainWindow):
    def __init__(self):

        # Setup GUI
        super().__init__()
        self.setupUi(self)
        self.setFixedSize(356, 304)
        self.br_dial = BrightnessDialogBox()
        self.br_dial.setFixedSize(350, 90)
        self.theme_actions = [self.red_, self.green_,
                              self.blue_, self.default_]
        self.cur_theme = self.default_
        self.last_brightness = 0
        self.connect_signals()
        self.show()

        # Setup calc
        self.last_operator = '+'
        self.left_operand = 0
        self.input_string = ''
        self.lcdnum = 0.

        # Init keyboard listener
        self.keyboard_listener = Listener(on_press=self.on_key_press)
        self.keyboard_listener.start()

    def on_key_press(self, key):
        """Triggered by keyboard event, calls num_input when numeric button or dot is pressed."""
        input_keys = '0123456789.'
        if key.char in input_keys:
            self.num_input(key.char)

    # noinspection PyUnresolvedReferences
    def connect_signals(self):
        """Connects signals to slots as required by QT"""

        # Brightness menu
        self.brightness_.triggered.connect(self.br_dial.show)
        self.br_dial.slider.valueChanged.connect(partial(self.change_theme, None))
        self.br_dial.buttonBox.clicked.connect(self.handle_dialog_box)

        # Theme menu
        for theme_action in self.theme_actions:
            theme_action.triggered.connect(partial(self.change_theme, theme_action))

        # Buttons
        num_buttons = [x for x in self.buttons_group.children() if isinstance(x, QPushButton)]
        for button in num_buttons:
            if re.match('num', button.objectName()):
                button.clicked.connect(partial(self.num_input, button.text()))
            else:
                button.clicked.connect(partial(self.handle_buttons, button.text()))

    def num_input(self, text):
        """Adds character to the input string and converts it to float"""
        if len(self.input_string) < 9:
            self.input_string += text
            try:
                self.lcdnum = float(self.input_string)
            except ValueError:
                self.input_string = self.input_string[:-1]
        self.lcd.display(self.lcdnum)

    def handle_buttons(self, text):
        """Calls method depending on button pressed"""
        func = {'√': self.one_operand,
                'x²': self.one_operand,
                '1/x': self.one_operand,
                '+': self.two_operands,
                '-': self.two_operands,
                '*': self.two_operands,
                '/': self.two_operands,
                'C': self.clear_,
                '=': self.equal}

        func[text](text)

    def one_operand(self, operator):
        """Performs calculations for one operand operations"""
        operation = {'√': lambda x: math.sqrt(x),
                     'x²': lambda x: pow(x, 2),
                     '1/x': lambda x: pow(x, -1)}

        try:
            self.lcdnum = operation[operator](self.lcdnum)
        except (ZeroDivisionError, ValueError, OverflowError):
            self.clear_()

        self.input_string = ''
        self.lcd.display(self.lcdnum)

    def two_operands(self, operator):
        """Performs calculations for two operands operations"""
        self.equal()
        self.left_operand = self.lcdnum
        self.input_string = ''
        self.last_operator = operator
        self.lcdnum = 0

    def clear_(self, *_):
        """Clears the memory and the screen"""
        self.left_operand = 0
        self.last_operator = '+'
        self.input_string = ''
        self.lcdnum = 0
        self.lcd.display(self.lcdnum)

    def equal(self, *_):
        """Ends input sequence for two operands operations, shows the outcome"""
        operation = {'+': lambda x, y: x+y,
                     '-': lambda x, y: x-y,
                     '/': lambda x, y: x/y,
                     '*': lambda x, y: x*y}

        # Return if nothing changed
        if self.last_operator == ' ':
            return

        # Try to perform the operation
        try:
            self.lcdnum = operation[self.last_operator](self.left_operand, self.lcdnum)
        except (ZeroDivisionError, ValueError, OverflowError):
            self.clear_()

        self.last_operator = ' '
        self.input_string = ''
        self.lcd.display(self.lcdnum)

    def change_theme(self, theme_action: QAction):
        """Changes the background color of application, called on selecting theme from menu or changing brightness"""

        # Change current theme if triggered by action menu, not by brightness bar
        if theme_action:
            self.cur_theme = theme_action
        key = self.cur_theme.objectName()
        color = {'default_': (240, 240, 240),
                 'green_': (50, 140, 50),
                 'blue_': (50, 50, 140),
                 'red_': (140, 50, 50)}

        # Adjust brightness if not default theme
        if key != 'default_':
            final_color = [x+self.br_dial.slider.value() for x in color[key]]
        else:
            final_color = color[key]

        # Change background color of QMainWindow
        palette = self.palette()
        brush = QBrush(QColor(*final_color))
        brush.setStyle(Qt.SolidPattern)
        palette.setBrush(QPalette.Active, QPalette.Window, brush)
        palette.setBrush(QPalette.Inactive, QPalette.Window, brush)
        self.setPalette(palette)

        # Uncheck positions in menu
        for action in self.theme_actions:
            action.setChecked(False)
        # Check current theme in menu
        self.cur_theme.setChecked(True)

    def handle_dialog_box(self, button):
        """Handle RESET, OK, and CANCEL buttons in brightness dialog box"""
        key = self.br_dial.buttonBox.standardButton(button)
        if key == QDialogButtonBox.Reset:
            self.br_dial.slider.setValue(0)
        elif key == QDialogButtonBox.Cancel:
            self.br_dial.slider.setValue(self.last_brightness)
        elif key == QDialogButtonBox.Ok:
            self.last_brightness = self.br_dial.slider.value()


app = QApplication(sys.argv)
calc = Calculator()
sys.exit(app.exec_())
